﻿#######################################
## Author: VuTD (vutd@runsystem.net) ##
## OS: Windows Server 2012           ##
#######################################

$gaMem=Get-WMIObject Win32_PhysicalMemory | Measure-Object -Property Capacity -Sum | %{$_.sum/1Mb}
$p="\\localhost\memory\available mbytes"
$gfMem=Get-Counter -Counter $p | Foreach-Object {$_.CounterSamples[0].CookedValue}
$guMem=$gaMem-$gfMem
$percentUsedMem=($guMem/$gaMem)*100
$dT=Get-Date
if($percentUsedMem -ge 97) {
   $wlog= "$dT Memory Used=$percentUsedMem% >= 97  [WARNING]: Restart IIS"
   Add-Content C:\check_memory.txt $wlog 
   #iisreset /restart
   iisreset /stop
   iisreset /start
}
else {
   Add-Content C:\check_memory.txt "$dT Memory Used=$percentUsedMem% System Nomal !" 
}
